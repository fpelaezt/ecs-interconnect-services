AWSTemplateFormatVersion: '2010-09-09'
Description: ECS Services Interconnection using Service Connect

Parameters:
  NetworkingStackName:
    Description: Networking Stack Name
    Type: String
    Default: app1-networking
  ECSClusterStackName:
    Description: ECS Cluster Stack Name
    Type: String
    Default: app1-ecsCluster
  AppName:
    Description: Application Name
    Type: String
    Default: app1
  ImageCore:
    Description: ECR Image Name
    Type: String
    Default: 'fpelaezt-app1-core'
  ImageTagCore:
    Description: Docker image tag for container
    Type: String
    Default: '1.009'
  ContainerNameCore:
    Description: Container name core
    Type: String
    Default: 'core'
  ContainerPortCore:
    Description: Container port core
    Type: String
    Default: '3000'

Resources:
  ######################
  # Environment Variables
  ######################
  NodeEnv:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/NODE_ENV
      Type: String
      Value: 'development'
  CoreBackendPort:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/BACKEND_PORT
      Type: String
      Value: '3000'
  DBPassword:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/DB_PASSWORD
      Type: String
      Value: 'mySecurePassword'
  DBMasterHost:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/MASTER_DB_HOST
      Type: String
      Value: 'myDbEndpoint'
  JWTTokenName:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/JWT_TOKEN_NAME
      Type: String
      Value: 'x-app1'
  JWTSecret:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/JWT_SECRET
      Type: String
      Value: 'SecreteKey2CreateTokens'
  JWTAcessExpirationHours:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/JWT_ACCESS_EXPIRATION_HOURS
      Type: String
      Value: '4h'
  JWTRefreshExpirationHours:
    Type: AWS::SSM::Parameter
    Properties:
      Name: /app1/JWT_REFRESH_EXPIRATION_HOURS
      Type: String
      Value: '4h'

  ContainerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: SG to test ping
      VpcId:
        Fn::ImportValue: !Sub '${NetworkingStackName}-vpc'
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 3000
          ToPort: 3100
          CidrIp: 10.0.0.0/20

  ######################
  # Task Definition Core
  ######################
  LogGroupCore:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, svc-core]]
      RetentionInDays: 7
  LogGroupCoreSC:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, serviceconnect, svc-core]]
      RetentionInDays: 7

  ECSTaskDefinitionCore:
    Type: 'AWS::ECS::TaskDefinition'
    DependsOn: LogGroupCore
    Properties:
      ContainerDefinitions:
        - Name: !Ref ContainerNameCore
          Image: !Sub '549799470133.dkr.ecr.us-east-1.amazonaws.com/${ImageCore}:${ImageTagCore}'
          Privileged: true
          Cpu: 0
          Essential: true
          Environment:
            - Name: SERVICE_NAME
              Value: 'svc-core'
            - Name: SVC_CORE_HOST
              Value: svc-core.serviceconnect.internal
            - Name: SVC_CORE_PORT
              Value: '3000'
            - Name: SVC_A_HOST
              Value: svc-a.serviceconnect.internal
            - Name: SVC_A_PORT
              Value: '3000'
            - Name: SVC_B_HOST
              Value: svc-b.serviceconnect.internal
            - Name: SVC_B_PORT
              Value: '3000'
          Secrets:
            - Name: 'NODE_ENV'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/NODE_ENV']]
            - Name: 'MASTER_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA1_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA2_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'BACKEND_PORT'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/BACKEND_PORT']]
            - Name: 'DB_PASSWORD'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/DB_PASSWORD']]
            - Name: 'JWT_TOKEN_NAME'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_TOKEN_NAME']]
            - Name: 'JWT_SECRET'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_SECRET']]
            - Name: 'JWT_ACCESS_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_ACCESS_EXPIRATION_HOURS']]
            - Name: 'JWT_REFRESH_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_REFRESH_EXPIRATION_HOURS']]
          PortMappings:
            - Name: svc-core
              ContainerPort: 3000
              Protocol: tcp
              HostPort: 3000
              AppProtocol: http
          HealthCheck:
            Command:
              - 'CMD-SHELL'
              - !Sub 'curl -f http://localhost:3000/svc-core/api/utils/health || exit 1'
            Interval: 30
            Retries: 3
            Timeout: 5
          LogConfiguration:
            LogDriver: 'awslogs'
            Options:
              awslogs-group: !Ref LogGroupCore
              awslogs-region: !Ref AWS::Region
              awslogs-stream-prefix: ecs
      Family: td-iac-app1-backend-core
      ExecutionRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskExecutionRole'
      TaskRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskRole'
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - 'EC2'
      Cpu: '128'
      Memory: '256'
      RuntimePlatform:
        CpuArchitecture: 'X86_64'
        OperatingSystemFamily: 'LINUX'

  ECSServiceCore:
    Type: AWS::ECS::Service
    Properties:
      Cluster: app1
      ServiceName: svc-core
      DeploymentConfiguration:
        MaximumPercent: 150
        MinimumHealthyPercent: 0
      DesiredCount: 1
      TaskDefinition: !Ref ECSTaskDefinitionCore
      EnableExecuteCommand: true
      NetworkConfiguration:
        AwsvpcConfiguration:
          Subnets:
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet01'
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet02'
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      ServiceConnectConfiguration:
        Enabled: true
        Namespace: app1.serviceconnect.internal
        Services:
          - PortName: svc-core
            ClientAliases:
              - Port: 3000
                DnsName: svc-core.serviceconnect.internal
        LogConfiguration:
          LogDriver: awslogs
          Options:
            awslogs-group: !Ref LogGroupCoreSC
            awslogs-region: !Ref AWS::Region
            awslogs-stream-prefix: svc-core

  ######################
  # Task Definition A
  ######################
  LogGroupA:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, svc-a]]
      RetentionInDays: 7
  LogGroupASC:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, serviceconnect, svc-a]]
      RetentionInDays: 7

  ECSTaskDefinitionA:
    Type: 'AWS::ECS::TaskDefinition'
    DependsOn: LogGroupA
    Properties:
      ContainerDefinitions:
        - Name: !Ref ContainerNameCore
          Image: !Sub '549799470133.dkr.ecr.us-east-1.amazonaws.com/${ImageCore}:${ImageTagCore}'
          Privileged: true
          Cpu: 0
          Essential: true
          Environment:
            - Name: SERVICE_NAME
              Value: 'svc-a'
            - Name: SVC_CORE_HOST
              Value: svc-core.serviceconnect.internal
            - Name: SVC_CORE_PORT
              Value: '3000'
            - Name: SVC_A_HOST
              Value: svc-a.serviceconnect.internal
            - Name: SVC_A_PORT
              Value: '3000'
            - Name: SVC_B_HOST
              Value: svc-b.serviceconnect.internal
            - Name: SVC_B_PORT
              Value: '3000'
          Secrets:
            - Name: 'NODE_ENV'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/NODE_ENV']]
            - Name: 'MASTER_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA1_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA2_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'BACKEND_PORT'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/BACKEND_PORT']]
            - Name: 'DB_PASSWORD'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/DB_PASSWORD']]
            - Name: 'JWT_TOKEN_NAME'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_TOKEN_NAME']]
            - Name: 'JWT_SECRET'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_SECRET']]
            - Name: 'JWT_ACCESS_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_ACCESS_EXPIRATION_HOURS']]
            - Name: 'JWT_REFRESH_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_REFRESH_EXPIRATION_HOURS']]
          PortMappings:
            - Name: svc-a
              ContainerPort: 3000
              Protocol: tcp
              HostPort: 3000
              AppProtocol: http
          HealthCheck:
            Command:
              - 'CMD-SHELL'
              - !Sub 'curl -f http://localhost:3000/svc-a/api/utils/health || exit 1'
            Interval: 30
            Retries: 3
            Timeout: 5
          LogConfiguration:
            LogDriver: 'awslogs'
            Options:
              awslogs-group: !Ref LogGroupA
              awslogs-region: !Ref AWS::Region
              awslogs-stream-prefix: ecs
      Family: td-iac-app1-backend-a
      ExecutionRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskExecutionRole'
      TaskRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskRole'
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - 'EC2'
      Cpu: '128'
      Memory: '256'
      RuntimePlatform:
        CpuArchitecture: 'X86_64'
        OperatingSystemFamily: 'LINUX'

  ECSServiceA:
    Type: AWS::ECS::Service
    DependsOn:
      - ECSServiceCore
    Properties:
      Cluster: app1
      ServiceName: svc-a
      DeploymentConfiguration:
        MaximumPercent: 150
        MinimumHealthyPercent: 0
      DesiredCount: 1
      TaskDefinition: !Ref ECSTaskDefinitionA
      EnableExecuteCommand: true
      NetworkConfiguration:
        AwsvpcConfiguration:
          Subnets:
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet01'
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet02'
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      ServiceConnectConfiguration:
        Enabled: true
        Namespace: app1.serviceconnect.internal
        Services:
          - PortName: svc-a
            ClientAliases:
              - Port: 3000
                DnsName: svc-a.serviceconnect.internal
        LogConfiguration:
          LogDriver: awslogs
          Options:
            awslogs-group: !Ref LogGroupASC
            awslogs-region: !Ref AWS::Region
            awslogs-stream-prefix: svc-a

  ######################
  # Task Definition B
  ######################
  LogGroupB:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, svc-b]]
      RetentionInDays: 7
  LogGroupBSC:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ["/", [/ecs, serviceconnect, svc-b]]
      RetentionInDays: 7

  ECSTaskDefinitionB:
    Type: 'AWS::ECS::TaskDefinition'
    DependsOn: LogGroupB
    Properties:
      ContainerDefinitions:
        - Name: !Ref ContainerNameCore
          Image: !Sub '549799470133.dkr.ecr.us-east-1.amazonaws.com/${ImageCore}:${ImageTagCore}'
          Privileged: true
          Cpu: 0
          Essential: true
          Environment:
            - Name: SERVICE_NAME
              Value: 'svc-b'
            - Name: SVC_CORE_HOST
              Value: svc-core.serviceconnect.internal
            - Name: SVC_CORE_PORT
              Value: '3000'
            - Name: SVC_A_HOST
              Value: svc-a.serviceconnect.internal
            - Name: SVC_A_PORT
              Value: '3000'
            - Name: SVC_B_HOST
              Value: svc-b.serviceconnect.internal
            - Name: SVC_B_PORT
              Value: '3000'
          Secrets:
            - Name: 'NODE_ENV'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/NODE_ENV']]
            - Name: 'MASTER_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA1_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'REPLICA2_DB_HOST'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/MASTER_DB_HOST']]
            - Name: 'BACKEND_PORT'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/BACKEND_PORT']]
            - Name: 'DB_PASSWORD'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/DB_PASSWORD']]
            - Name: 'JWT_TOKEN_NAME'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_TOKEN_NAME']]
            - Name: 'JWT_SECRET'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_SECRET']]
            - Name: 'JWT_ACCESS_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_ACCESS_EXPIRATION_HOURS']]
            - Name: 'JWT_REFRESH_EXPIRATION_HOURS'
              ValueFrom: !Join [':', ['arn:aws:ssm', !Ref AWS::Region, !Ref AWS::AccountId, !Sub 'parameter/app1/JWT_REFRESH_EXPIRATION_HOURS']]
          PortMappings:
            - Name: svc-b
              ContainerPort: 3000
              Protocol: tcp
              HostPort: 3000
              AppProtocol: http
          HealthCheck:
            Command:
              - 'CMD-SHELL'
              - !Sub 'curl -f http://localhost:3000/svc-b/api/utils/health || exit 1'
            Interval: 30
            Retries: 3
            Timeout: 5
          LogConfiguration:
            LogDriver: 'awslogs'
            Options:
              awslogs-group: !Ref LogGroupB
              awslogs-region: !Ref AWS::Region
              awslogs-stream-prefix: ecs
      Family: td-iac-app1-backend-b
      ExecutionRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskExecutionRole'
      TaskRoleArn:
        Fn::ImportValue: !Sub '${ECSClusterStackName}-ECSTaskRole'
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - 'EC2'
      Cpu: '128'
      Memory: '256'
      RuntimePlatform:
        CpuArchitecture: 'X86_64'
        OperatingSystemFamily: 'LINUX'

  ECSServiceB:
    Type: AWS::ECS::Service
    DependsOn:
      - ECSServiceA
    Properties:
      Cluster: app1
      ServiceName: svc-b
      DeploymentConfiguration:
        MaximumPercent: 150
        MinimumHealthyPercent: 0
      DesiredCount: 1
      TaskDefinition: !Ref ECSTaskDefinitionB
      EnableExecuteCommand: true
      NetworkConfiguration:
        AwsvpcConfiguration:
          Subnets:
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet01'
            - Fn::ImportValue: !Sub '${NetworkingStackName}-PublicSubnet02'
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      ServiceConnectConfiguration:
        Enabled: true
        Namespace: app1.serviceconnect.internal
        Services:
          - PortName: svc-b
            ClientAliases:
              - Port: 3000
                DnsName: svc-b.serviceconnect.internal
        LogConfiguration:
          LogDriver: awslogs
          Options:
            awslogs-group: !Ref LogGroupBSC
            awslogs-region: !Ref AWS::Region
            awslogs-stream-prefix: svc-b

