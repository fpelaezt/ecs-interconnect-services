# App1 Infrastructure As Code (IAC)

This project contains the necesary cloudformation templates to create resources that would help to understand how to interconnect different containers or micro-services using Elastic Container Service (ECS).

## Pre-requisites

* An AWS Account
* A user with administrator access for creating resources
* A CLI environment to run the templates. This could be your local laptop or a Cloud9 environment

## Create Infrastructure

Create the following stacks in order.

### Networking (Stack = app1-networking)

This template creates:

* VPC
* Two public subnets
* One Internet Gateway (IGW)
* Routing table to internet

```
aws cloudformation create-stack --stack-name app1-networking --template-body file://./backend/0-networking.yaml
```

### ECS Cluster (Stack = app1-ecsCluster)

This template creates:

* Roles (Task Execution Role, Task Role, ECS Service Role, EC2 Role, Instance Profile)
* ECS Cluster
* Requires security groups
* AutoScaling (Launch Template, AutoScaling Group)

```
aws cloudformation create-stack --stack-name app1-ecsCluster --template-body file://./backend/1-ecsCluster.yaml --capabilities CAPABILITY_IAM
```

After creating the networking and cluster stack, our environment will look like this

![Image](./images/ECS-Services-Resources.drawio.png)


## ECS Services

Now that we have our infrastructure it's time to deploy our services and validate how they can interact with each other.


### Interconnecting Services using ALB

Initially we're going to create three services behind a load balancer.

```
aws cloudformation create-stack --stack-name app1-ecsServicesALB --template-body file://./backend/2-ecsServicesALB.yaml
```

The stack will create this

![Image](./images/ECS-Services-ALB.drawio.png)

Take into account:
* Three services are created (SVC-CORE, SVC-A, SVC-B)
* An Internal ALB is created with a Listener on Port 3000
* Every service is registered on the ALB as a target group under the same Listener with a path route based
    * /svc-core with listener priority 1
    * /svc-a with listener priority 2
    * /svc-a with listener priority 3


#### Testing Interconnection

Using EC2 System Manager, connect to one of the EC2 and then open a shell inside one of the Containers.

```
docker exec -it <container_id> /bin/bash
```

Identify which service I'm connected to by running this

```
env | grep SERVICE_NAME
```

You could check that all services are replying correctly accessing the healthcheck route

```
curl http://$ALB_HOST:$ALB_PORT/svc-core/api/utils/health
curl http://$ALB_HOST:$ALB_PORT/svc-a/api/utils/health
curl http://$ALB_HOST:$ALB_PORT/svc-b/api/utils/health
```

You should get the following reply

```
{
    service_name: SERVICE_NAME,
    uptime: UPTIME_SECONDS,
    message: 'OK',
    timestamp: DATE,
}
```

Every time you hit one of healthcheck URL you can see it on the container logs or in the CloudWatch Log Group of each service

#### Extra Activities

You can dig further on the configuration paying attention to:
* The ALB configuration, Listener and how target groups are created
* Update the stack configuration changing specific parameters like desire services, ec2 instance type and how this impacts the deployments
* Check Cloudwatch Log groups
* Check Cloudwatch metrics for targetgroups

### Interconnecting Services using Service Connect

Let's now move to how to deploy the same micro-services and allow them to interconnect using Service Connect instead of an ALB.

Remove the previous stack before creating this

```
aws cloudformation create-stack --stack-name app1-ecsServicesConnect --template-body file://./backend/3-ecsServicesConnect.yaml
```

Our environment will now have this

![Image](./images/ECS-Services-ServiceConnect.drawio.png)


#### Testing Interconnection

Using the same procedure described in the previous section, connect to one of the Containers and run the following commands. Pay attention to connect to the right container and not to the Envoy or Pause.

```
docker exec -it <container_id> /bin/bash
```

Connect to the CORE container and validate you are connected on the right service using

```
env | grep SERVICE_NAME
```

Run Connectivity tests againts other services

```
curl http://$SVC_CORE_HOST:$SVC_CORE_PORT/svc-core/api/utils/health
curl http://$SVC_A_HOST:$SVC_A_PORT/svc-a/api/utils/health
curl http://$SVC_B_HOST:$SVC_B_PORT/svc-b/api/utils/health
```

If you connect to the SVC-CORE, you will notice that healthcheck for SVC-CORE run ok, but no the others. This happens because on the cloudformation templates the services are created in this order SVC-CORE --> SVC-A --> SVC-C. So when SVC-CORE is created is not yet aware of the other services. To fix this, [you will need to reploy the service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/service-connect-concepts.html#service-connect-concepts-deploy). You can force a new deployment updating the service from the console.

#### Extra Activities

You can dig further on the configuration paying attention to:
* The /etc/hosts file for each container shows which services are available for the current server
* Take into account that now each container has an Envoy container
* Validate new metrics created on CloudWatch for each service
